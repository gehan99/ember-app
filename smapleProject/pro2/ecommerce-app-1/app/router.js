import EmberRouter from '@ember/routing/router';
import config from 'ecommerce-app-1/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('about');
  this.route('contact');
  this.route('home');
  this.route('cart');
  this.route('orders');
  this.route('products');
});
